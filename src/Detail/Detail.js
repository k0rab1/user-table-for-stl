import React from 'react'
import Form from '../Form/Form'

const Detail = props => {
  const { id } = props.match.params
    return (
      <div>
        <Form id = { id } />
      </div>
    )
}

export default Detail
