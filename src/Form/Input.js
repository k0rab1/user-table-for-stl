import React from 'react'

const Input = props => {
  return (
    <div className='form-control'>
      <label htmlFor={props.name}>{props.name}</label>
      <input
        className="form-control"
        id={props.name}
        name={props.name}
        type='text'
        value={props.value}
        onChange={props.handleChange}
      />
    </div>
  )
}

export default Input