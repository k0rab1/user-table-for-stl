import React, { useContext } from 'react'
import Input from './Input'
import Select from './Select'
import './style.css'
import Context from '../Context'
import { NavLink } from 'react-router-dom'

const Form = props => {

  const { editUser, users } = useContext(Context)
  const user = users[props.id]

  return (
    <form>
      {
        Object.keys(user).map((prop, index) => {
          if (prop === 'id') return
          if (prop === 'country') {
            return (
              <Select
                key={index}
                name={prop}
                value={user.[prop]}
                handleChange={event => editUser(event.target.value, props.id, prop)}
              />
            )
          } else {
            return (
              <Input
                key={index}
                name={prop}
                value={user.[prop]}
                handleChange={event => editUser(event.target.value, props.id, prop)}
              />
            )
          }
        })
      }
      <div class='form-control form-control__btn'>
        <NavLink className='btn' to="/">Submit</NavLink>
      </div>
    </form>
  )

}

export default Form