import React from 'react'

const Select = props => {
  return (
    <div className='form-control'>
      <label htmlFor={props.name}>{props.name}</label>
      <select
        id={props.name}
        name={props.name}
        value={props.value}
        onChange={props.handleChange}
        className="form-control"
      >
        <option value="USA">
          USA
        </option>
        <option value="Russia">
          Russia
        </option>
        <option value="Australia">
          Australia
        </option>
      </select>
    </div>
  )
}

export default Select
