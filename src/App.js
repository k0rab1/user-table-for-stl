import React, { useState } from 'react'
import { BrowserRouter, Route } from 'react-router-dom'
import Context from './Context'
import Table from './UserTable/Table'
import Detail from './Detail/Detail'
import UsersList from './UserTable/UsersList'

function App () {
  const [users, setUsers] = useState(UsersList)

  const removeUser = id => setUsers(users.filter(users => users.id !== id))

  const sortUsers = propName => {
    const oldUser = [...users]

    if (users.sorted === true){
      oldUser.sort((a, b) => a[propName] < b[propName] ? 1 : -1)
      oldUser.sorted = false
    } else{
      oldUser.sort((a, b) => a[propName] > b[propName] ? 1 : -1)
      oldUser.sorted = true
    }

    setUsers(oldUser)
  }

  const editUser = (name, index, prop) => {
    const user = users[index]
    user.[prop] = name

    const newUsers = [...users]
    newUsers[index] = user

    setUsers(newUsers)
  }



  return (
    <Context.Provider value={{ removeUser, editUser, sortUsers, users }}>
      <div className='wrapper'>
        <BrowserRouter>
          <h1>Table of Users.</h1>

          <Route exact path="/" component={Table.bind(null, users)}/>
          <Route exact path="/detail/:id" component={Detail}/>
        </BrowserRouter>
      </div>
    </Context.Provider>
  )
}

export default App
