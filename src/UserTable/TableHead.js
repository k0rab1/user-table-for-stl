import React, { useContext } from 'react'
import Context from '../Context'
import './style.css'

const TableHead = () => {

  const { sortUsers } = useContext(Context)

  return (
    <thead>
    <tr>
      <th>
        <button className='btn' onClick={sortUsers.bind(null, 'id')}>#</button>
      </th>
      <th>
        <button className='btn' onClick={sortUsers.bind(null, 'name')}>Name</button>
      </th>

      <th>
        <button className='btn' onClick={sortUsers.bind(null, 'email')}>Email</button>
      </th>

      <th>
        <button className='btn' onClick={sortUsers.bind(null, 'country')}>Country</button>
      </th>

      <th>
        <button className='btn' onClick={sortUsers.bind(null, 'phone')}>Phone</button>
      </th>

      <th>
        <button className='btn' onClick={sortUsers.bind(null, 'age')}>Age</button>
      </th>
      <th>Edit/Del</th>
    </tr>
    </thead>
  )
}

export default TableHead