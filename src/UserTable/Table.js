import React from 'react'
import TableHead  from './TableHead'
import User from './User'
import './style.css'

const Table = users => {

  return (
    <table>
      <TableHead />
      <tbody>
      {users.map((user, index) => {
        return (
          <User
            user={user}
            key={user.id}
            index={index}
          />
        )
      })}
      </tbody>
    </table>
  )

}

export default Table