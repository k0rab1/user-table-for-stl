import React, { useContext } from 'react'
import Context from '../Context'
import { NavLink } from 'react-router-dom'

function User ({ user, index }) {
  const { removeUser } = useContext(Context)

  return (
    <tr>
      <td>{user.id}</td>
      <td className="name">{user.name}</td>
      <td className="email">{user.email}</td>
      <td className="country">{user.country}</td>
      <td className="phone">{user.phone}</td>
      <td className="age">{user.age}</td>
      <td className="buttons">
        <NavLink to={`/detail/${index}`} className='btn edit'>Edit</NavLink>
        <button className='remove  btn' onClick={removeUser.bind(null, user.id)}>&times;</button>
      </td>
    </tr>
  )
}

export default User